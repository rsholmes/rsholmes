# Categorized list of repositories

## Analog Output Eurorack modules and panels
* [MFOS-SVF](https://gitlab.com/rsholmes/MFOS-SVF) Eurorack adaptation of the MFOS "Oldie But Goody" state variable filter

## Analog Output Kosmo modules and panels
* [8StepSequencer](https://gitlab.com/rsholmes/8StepSequencer) 8 step control voltage sequencer based on LMNC design, with a number of modifications
* [and](https://gitlab.com/rsholmes/and) Add Noise Drum, tone and noise based analog drum module
* [arpenvfol](https://gitlab.com/rsholmes/arpenvfol) ARP 2600 based envelope follower 
* [arrm](https://gitlab.com/rsholmes/arrm) Active Real Ring Modulator by Ken Stone (CGS)
* [attenuverters](https://gitlab.com/rsholmes/attenuverters) Attenuverters with summing
* [audiomixer](https://gitlab.com/rsholmes/audiomixer) Audio mixer synth module
* [bong0](https://gitlab.com/rsholmes/bong0) Dual Kosmo format version of the Nonlinear Circuits Bong0 drum module
* [clock](https://gitlab.com/rsholmes/clock) Clock synth module
* [cvt](https://gitlab.com/rsholmes/cvt) Converts voltage levels
* [dc_mixer](https://gitlab.com/rsholmes/dc_mixer) 3 input DC mixer Kosmo format synth module
* [displacer](https://gitlab.com/rsholmes/displacer) Waveshaper with a bunch of other uses
* [dso138](https://gitlab.com/rsholmes/dso138) Oscilloscope module from Jye Tech DSO138
* [dvca](https://gitlab.com/rsholmes/dvca) MFOS dual VCA in Kosmo format
* [exped](https://gitlab.com/rsholmes/exped) Expression pedal interface synth module
* [ffbmod](https://gitlab.com/rsholmes/ffbmod) YuSynth Fixed Filter Bank, retuned, with board mounted sliders and jacks
* [frontpanels](https://gitlab.com/rsholmes/frontpanels) Front panels for various synth modules
* [GateGrinder](https://gitlab.com/rsholmes/GateGrinder) Synth module to generate clocks and act on triggers and gates
* [gearseq](https://gitlab.com/rsholmes/gearseq) Voltage controlled trigger sequencer with gap, Euclidean, ADC, and random algorithms
* [herovco](https://gitlab.com/rsholmes/herovco) Full featured 3340 based VCO
* [irinput](https://gitlab.com/rsholmes/irinput) Modification of Barton IR Input
* [joystick](https://gitlab.com/rsholmes/joystick) Joystick CV controller with adjustable ranges and offsets
* [kdlfo](https://gitlab.com/rsholmes/kdlfo) Shape variable dual LFO
* [KosmoSlopes](https://gitlab.com/rsholmes/KosmoSlopes) Kosmo Version of the Kassutronics Slope Module
* [megamodule](https://gitlab.com/rsholmes/megamodule) Toy voice changing megaphone bent into synth module
* [MCVI](https://gitlab.com/rsholmes/MCVI) Simple MIDI to CV synth module
* [MFOS_VC_LFO](https://gitlab.com/rsholmes/MFOS_VC_LFO) Auxiliary board and Kosmo format front panel for MFOS VC LFO, supplying sync and power header
* [mikrokosmosii](https://gitlab.com/rsholmes/mikrokosmosii) Piezo element amplifier inspired by Music Thing Modular's Mikrophonie (supersedes earlier [Mikrokosmos](https://gitlab.com/rsholmes/Mikrokosmos) version)
* [mtmreverb](https://gitlab.com/rsholmes/mtmreverb) Spring and "brick" reverb driver
* [mults](https://gitlab.com/rsholmes/mults) Dual buffered multiples
* [NoiseBells](https://gitlab.com/rsholmes/NoiseBells) Percussion/drone module based on Hackaday CMOS Noise article
* [nucleardecay](https://gitlab.com/rsholmes/nucleardecay) TimMLN's module based on a Geiger counter
* [pmf](https://gitlab.com/rsholmes/pmf) Pole mixing multimode filter
* [powerdisplay](https://gitlab.com/rsholmes/powerdisplay) Power display and front panel power header
* [precadsr](https://gitlab.com/rsholmes/precadsr) Kassutronics Precision ADSR with retriggering and looping modifications
* [QuantizerModule](https://gitlab.com/rsholmes/QuantizerModule) Dual CV quantizer with choice of ~72 scales
* [ringer](https://gitlab.com/rsholmes/ringer) Analog multiplier
* [rungler](https://gitlab.com/rsholmes/rungler) Rungler (chaotic control voltage sequence generator)
* [sidekickvco](https://gitlab.com/rsholmes/sidekickvco) Subsidiary 3340 based VCO
* [StereoOutKosmo](https://gitlab.com/rsholmes/StereoOutKosmo) Stereo output synthesizer module in Kosmo format
* [subosc](https://gitlab.com/rsholmes/subosc) Suboscillator by Carmelo Azarello
* [vcapf](https://gitlab.com/rsholmes/vcapf) Low frequency voltage controlled all pass filter
* [voltproc](https://gitlab.com/rsholmes/voltproc) Voltage processor with summers and lag
* [WaveShaper](https://gitlab.com/rsholmes/WaveShaper) Barton 4046 wave shaper modified to add voltage control and variable gain
* [wsgmodule](https://gitlab.com/rsholmes/wsgmodule) Kosmo format synth module based on the Music From Outer Space Weird Sound Generator
* [yash](https://gitlab.com/rsholmes/yash) Sample and Hold by René Schmitz

## Other synth related hardware
* [busboard](https://gitlab.com/rsholmes/busboard) Eurorack/Kosmo Busboards
* [cabletester](https://gitlab.com/rsholmes/cabletester) Arduino 10/16 pin Eurorack IDC Power Cable Tester
* [dac_ino](https://gitlab.com/rsholmes/dac_ino) Arduino CV/Gate I/O shield and its software library
* [gates-pulses-switch](https://gitlab.com/rsholmes/gates-pulses-switch) Daughterboard to allow Turing Machine/Halting Problem expanders to switch between GATES and PULSES
* [midisnoop](https://gitlab.com/rsholmes/midisnoop) For looking at incoming MIDI messages
* [plexdaus](https://gitlab.com/rsholmes/plexdaus) Daughterboards intended to lower gate requirements for the LMNC 4051 Plexquencer synth module
* [powerbreakout](https://gitlab.com/rsholmes/powerbreakout) Euro/Kosmo power breakout
* [Protoboard](https://gitlab.com/rsholmes/Protoboard) Eurorack/Kosmo powered prototyping boards
* [ribcon](https://gitlab.com/rsholmes/ribcon) Interface and software for resistive ribbon controller
* [ww_supply](https://gitlab.com/rsholmes/ww_supply) ±12V power supply for synthesizers

## Other hardware
* [opamptester](https://gitlab.com/rsholmes/opamptester) Stoopid op amp tester

## KiCad templates, libraries, and software
* [aoKicad](https://gitlab.com/rsholmes/aoKicad) KiCad symbol and footprint libraries I use all the time
* [kdocgen](https://gitlab.com/rsholmes/kdocgen) Python script to generate outputs using the KiCad CLI
* [kicadfootprints](https://gitlab.com/rsholmes/kicadfootprints) Custom KiCad footprints. Repository superseded by [aoKicad](https://gitlab.com/rsholmes/aoKicad).
* [kicadsymbols](https://gitlab.com/rsholmes/kicadsymbols) Various symbols I've created or adapted. Repository superseded by [aoKicad](https://gitlab.com/rsholmes/aoKicad).
* [Kosmo_panel](https://gitlab.com/rsholmes/Kosmo_panel) KiCad footprint library for Kosmo panel elements
* [Kosmo_panel_templates](https://gitlab.com/rsholmes/Kosmo_panel_templates) KiCad templates for Kosmo panel projects
* [bomscripts](https://gitlab.com/rsholmes/bomscripts) Plugins for BOMs

## Music and synth related software
* [bestintervals.py](https://gitlab.com/rsholmes/bestintervals.py) Python script for exploring approximations to just (or other) intervals in equal divisions of the octave (EDOs) and linear scales
* [kmg_csv2json](https://gitlab.com/rsholmes/kmg_csv2json) Python script to generate kosmodulargrid entries from CSV file
* [playoeis](https://gitlab.com/rsholmes/playoeis) Python script to play an OEIS sequence as a MIDI sequence

## Mathematical recreations
* [nclique](https://gitlab.com/rsholmes/nclique) Find maximal length sets of words each n letters long with no letters repeated
* [ssoma](https://gitlab.com/rsholmes/ssoma) Soma Cube puzzle solver

## Python libraries
* [oeislib](https://gitlab.com/rsholmes/oeislib) Python library for OEIS (Online Encyclopedia of Integer Sequences)
* [primeclassify](https://gitlab.com/rsholmes/primeclassify) Library of functions to classify prime numbers

## Other projects
* [rshbat](https://gitlab.com/rsholmes/rshbat) theme for bat

## Largely unmodified forks of misc repos
* [breadboard-friends](https://gitlab.com/rsholmes/breadboard-friends) A collection of cute breakout boards riding solderless breadboards, mostly for analog audio works
* [cvpal](https://gitlab.com/rsholmes/cvpal) Cheap and cheerful 2-channel USB to CV/Gate interface
* [Electrophone](https://gitlab.com/rsholmes/Electrophone) Guitar pickup module
* [kosmodulargrid](https://gitlab.com/rsholmes/kosmodulargrid) Site for Kosmo modules
* [KosmoPulse](https://gitlab.com/rsholmes/KosmoPulse) Music Thing Modular Pulse Expander for the Turing Machine converted to Kosmo Format
* [KosmoTuringMachine](https://gitlab.com/rsholmes/KosmoTuringMachine) Music Thing Modular Turing Machine converted to Kosmo Format
* [KosmoVolts](https://gitlab.com/rsholmes/KosmoVolts) Music Thing Modular Volts Expander for the Turing Machine converted to Kosmo Format
* [midipal](https://gitlab.com/rsholmes/midipal) MIDI swiss army knife
* [module_tester](https://gitlab.com/rsholmes/module_tester) Test signal generator for Eurorack module
* [mutable_eurorack](https://gitlab.com/rsholmes/mutable_eurorack) MI Eurorack modules
* [SimpleEQ](https://gitlab.com/rsholmes/SimpleEQ) SMD Eurorack EQ Module

## (Temporarily?) abandoned projects
<!-- * [8dc](https://gitlab.com/rsholmes/8dc) CV to ADC Eurorack module, inspired by Look Mum No Computer's 123ADC -->
* [Arpeggiator](https://gitlab.com/rsholmes/Arpeggiator) Kosmo format (Eurorack compatible) arpeggiator module
* [drum6](https://gitlab.com/rsholmes/drum6) Ridiculous bending and repurposing of First Act drum pad as Eurorack module and maybe controller

## Gratuitous recursion
* [rsholmes](https://gitlab.com/rsholmes/rsholmes) Gratuitous recursion
